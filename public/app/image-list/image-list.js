/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * Represents the image list page for admin
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */

(function () {
    'use strict';

    angular.module('app')
        .config(function ($stateProvider) {
            $stateProvider
                .state('imageList', {
                    admin: true,
                    url: '^/image-list',
                    templateUrl: 'app/image-list/image-list.html',
                    controller: 'ImageListCtrl as vm'
                });
        })
        .controller('ImageListCtrl', function ($rootScope, $state, config, NgTableParams, API, util) {
            var vm = this;
                
            vm.tableParams = new NgTableParams({
                page: 1,
                count: config.PAGE_SIZES[0]
            }, {
                counts: config.PAGE_SIZES,
                paginationMaxBlocks: 3,
                paginationMinBlocks: 2,
                getData: function(params) {
                    var query = {
                        limit: params.count(),
                        offset: (params.page() - 1) * params.count()
                    };
                    return API.getImages(query).$promise.then(function(data) {
                        params.total(data.total);
                        vm.total = data.total;
                        vm.images = data.items;
                        return data.items;
                    }, function(){
                        util.showAlert('Error', 'Failed to load the images');
                    });
                }
            });
        });

})();