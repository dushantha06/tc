/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * The application configure
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */

(function () {
    'use strict';

    var config = {
        // table page size in image list table
        PAGE_SIZES: [10, 25, 50],
        // marker size of stalk and weed, this is percentage size
        MARKER_SIZE: 0.03,
        // the height/width rate of canvas
        CANVAS_HEIGHT_WIDTH_RATE: 3/4,
        // the minimum zoom scale of canvas
        CANVAS_MIN_SCALE: 0.5,
        // the marker stroke width of stalk and weed in pixel
        MARKER_STROKE_WIDTH: 1,
        // the marker color of stalk
        STALK_MARKER_COLOR: 'red',
        // the marker color of weed
        WEED_MARKER_COLOR: 'yellow',
        // the image base URL
        IMAGE_BASE_URL: '/images/',
        // when drag amount is less than this value (in pixel), treat drag as click
        DRAG_AMOUNT_TREAT_AS_CLICK: 5,
        // the max image size in MB to upload
        MAX_IMAGE_SIZE: 10
    };

    angular.module('app')
        .constant("config", config);
})();