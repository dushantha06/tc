/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * Represents main application module
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */

(function () {
    'use strict';

    angular.module('app', ['ui.router', 'ui.bootstrap', 'ngResource', 'angularFileUpload', 'ngTable'])
        .config(function ($urlRouterProvider) {
            $urlRouterProvider
                .otherwise('/login');
        })
        .run(function ($rootScope, $state, config) {
            $rootScope.imageBaseUrl = config.IMAGE_BASE_URL;
            $rootScope.currentUser = {};
            $rootScope.$on('$stateChangeStart', function(event, toState) {
                if (!toState || toState.public) {
                    return;
                }
                if (!$rootScope.currentUser.authenticated) {
                    event.preventDefault();
                    $state.go("login");
                    return;
                }
                if (!$rootScope.currentUser.admin && toState.admin) {
                    event.preventDefault();
                    return;
                }
            });
        });

})();