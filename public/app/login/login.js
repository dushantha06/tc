/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * Represents the login page
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */

(function () {
    'use strict';

    angular.module('app')
        .config(function ($stateProvider) {
            $stateProvider
                .state('login', {
                    public: true,
                    url: '^/login',
                    templateUrl: 'app/login/login.html',
                    controller: 'LoginCtrl as vm'
                });
        })
        .controller('LoginCtrl', function ($rootScope, $state, API) {
            var vm = this;
            
            // for test
            vm.username = 'admin';
            vm.password = 'admin';

            vm.login = login;
            vm.logging = false;
            // login action
            function login(form) {
                vm.showError = false;
                if (vm.logging) {
                    return;
                }
                if (form.$invalid) {
                    vm.validate = true;
                    return;
                }
                vm.logging = true;
                API.login({
                    username: vm.username,
                    password: vm.password
                }).$promise.then(function(res){
                    $rootScope.currentUser = {
                        authenticated: true,
                        username: res.username,
                        admin: res.admin
                    };
                    if(res.admin){
                        $state.go('imageList');
                    } else {
                        $state.go('imageDetail');
                    }
                    vm.logging = true;
                }, function(){
                    vm.showError = true;
                    vm.logging = true;
                });
            }
        });
})();