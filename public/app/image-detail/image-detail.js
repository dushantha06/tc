/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * Represents the image detail view page
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */

(function () {
    'use strict';

    angular.module('app')
        .config(function ($stateProvider) {
            $stateProvider
                .state('imageDetail', {
                    url: '^/image-detail/:id',
                    templateUrl: 'app/image-detail/image-detail.html',
                    controller: 'ImageDetailCtrl as vm'
                });
        })
        .controller('ImageDetailCtrl', function ($rootScope, $stateParams, API, $state, util) {
            var vm = this;
            var imageId = parseInt($stateParams.id);
            vm.markingStalks = false;
            vm.markingWeeds = false;
            vm.submitMarks = submitMarks;
            loadImageData();
            // load image and marks
            function loadImageData(){
                API.getImage({id: isNaN(imageId) ? null : imageId}).$promise.then(function(res){
                    if(imageId || (res.left > 0 && res.image)){
                        vm.image = res.image;
                        imageId = vm.image.id;
                        if(!vm.image.mark) {
                            vm.image.mark = {
                                note: '',
                                stalks: [],
                                weeds: []
                            };
                        }
                    } else {
                        util.showAlert('All Done', 'All images have been marked');
                    }
                });
            }
            // submit marks
            function submitMarks(){
                if(vm.image && vm.image.mark && 
                    (vm.image.mark.note || vm.image.mark.stalks.length || vm.image.mark.weeds.length)){
                    var marksForSubmit = {
                        note: vm.image.mark.note,
                        stalks: [],
                        weeds: []
                    };
                    vm.image.mark.stalks.forEach(function(stalk){
                        marksForSubmit.stalks.push({x: stalk.x, y: stalk.y});
                    });
                    vm.image.mark.weeds.forEach(function(weed){
                        marksForSubmit.weeds.push({x: weed.x, y: weed.y});
                    });                    
                    API.uploadMarks({id: imageId}, marksForSubmit).$promise.then(function(){
                        util.showAlert('Success', 'Success to save the marks and note, now next image to mark')
                            .result.then(function(){
                                $state.go('imageDetail', {id: null}, {reload: true});
                            });
                    }, function(){
                        util.showAlert('Error', 'Failed to save the marks and note');
                    });
                } else {
                    util.showAlert('Info', 'You should mark some stalks and weeds or input some notes before submit');
                }
            }
        })
        .directive('ngcCanvas', function(config) {
            return {
                restrict: 'EA',
                template: '<div id="canvas-container"/>',
                replace: true,
                scope: {
                    image: '=',
                    markingStalks: '=',
                    markingWeeds: '='
                },
                link: function(scope, element, attributes) {
                    var stage = null;
                    var layer = null;
                    var watcher = scope.$watch('image.fileName', function(imageFile){
                        if(imageFile){
                            watcher();
                            initCanvas();
                        }
                    });
                    // use mouse wheel to zoom in computer
                    $("#canvas-container").mousewheel(function(e, delta){
                        if(layer){
                            var zoomAmount = delta * 0.01;
                            var scale = layer.getScale().x + zoomAmount;
                            scale = scale < config.CANVAS_MIN_SCALE ? config.CANVAS_MIN_SCALE : scale;
                            layer.scale({x: scale, y: scale});
                            layer.draw();
                            e.preventDefault();
                        }
                    });
                    var hammer = new Hammer(document.getElementById("canvas-container"));
                    hammer.get('pinch').set({ enable: true });
                    var startScale;
                    // use pin gesture to zoom in mobile device or tablet
                    hammer.on("pinchstart", function (e) {
                        if(layer){
                            startScale = layer.getScale().x;
                            e.preventDefault();
                        }
                    });
                    // use pin gesture to zoom in mobile device or tablet
                    hammer.on("pinchmove", function (e) {
                        if(layer && startScale){
                            var scale = startScale * e.scale;
                            scale = scale < config.CANVAS_MIN_SCALE ? config.CANVAS_MIN_SCALE : scale;
                            layer.scale({x: scale, y: scale});
                            layer.draw();
                            e.preventDefault();
                        }
                    });
                    /**
                     * Find a shape which the given point inside this shape
                     * @param {Object} point the given point
                     * @param {Array} shapeList the shape list
                     * @param {Number} size the shape size
                     * @returns {Number} the found shape's index, -1 if not found
                     */
                    function findShapeAtPoint(point, shapeList, size){
                        var index = -1;
                        var halfSize = size / 2;
                        for(var i = 0; i < shapeList.length; i++){
                            var shape = shapeList[i];
                            if(point.x > shape.x - halfSize && point.x < shape.x + halfSize && 
                                point.y > shape.y - halfSize && point.y < shape.y + halfSize){
                                index = i;
                                break;
                            }
                        }
                        return index;
                    }
                    /**
                     * Init the canvas
                     */                    
                    function initCanvas(){
                        var imageFile = scope.image.fileName;
                        var width = $(element).width();
                        var factor = config.CANVAS_HEIGHT_WIDTH_RATE;
                        var height = width * factor;
                        var markSize = config.MARKER_SIZE * width;
                        var imageObj = new Image();
                        var imageWidth = 0;
                        var imageHeight = 0;
                        var imageDrawSize = null;
                        if(stage){
                            stage.clear();
                        }
                        stage = new Kinetic.Stage({
                            container : 'canvas-container',
                            width : width,
                            height : height
                        });
                        layer = new Kinetic.Layer({
                            draggable: true
                        });
                        var backgroundRect = new Kinetic.Rect({
                            x: -5 * width,
                            y: -5 * height,
                            width: 10 * width,
                            height: 10 * height,
                            fill: '#ccc'
                        });
                        layer.add(backgroundRect);
                        stage.add(layer);
                        imageObj.src = config.IMAGE_BASE_URL + imageFile;
                        // draw canvas and register events when image loaded
                        imageObj.onload = function() {
                            imageWidth = imageObj.width;
                            imageHeight = imageObj.height;
                            var imageFactor = imageHeight/imageWidth;
                            var imageScale = imageFactor > factor ? imageHeight/height : imageWidth/width;
                            var imageStartPoint = {
                                x: imageFactor > factor ? (width - imageWidth/imageHeight*height)/2 : 0,
                                y: imageFactor > factor ? 0 : (height - imageHeight/imageWidth*width)/2,
                            };
                            imageDrawSize = {
                                width: width - 2 * imageStartPoint.x,
                                height: height - 2 * imageStartPoint.y,
                            };
                            var image = new Kinetic.Image({
                                x: imageStartPoint.x,
                                y: imageStartPoint.y,
                                width: imageDrawSize.width,
                                height: imageDrawSize.height,
                                image: imageObj
                            });
                            layer.add(image);
                            layer.draw();
                            scope.image.mark.stalks.forEach(function(stalk){
                                stalk.shape =  new Kinetic.Rect({
                                    x: stalk.x/imageWidth*imageDrawSize.width + imageStartPoint.x - markSize/2,
                                    y: stalk.y/imageHeight*imageDrawSize.height + imageStartPoint.y - markSize/2,
                                    width: markSize,
                                    height: markSize,
                                    stroke: config.STALK_MARKER_COLOR,
                                    strokeWidth: config.MARKER_STROKE_WIDTH
                                });
                                layer.add(stalk.shape);
                            });
                            scope.image.mark.weeds.forEach(function(weed){
                                weed.shape =  new Kinetic.Circle({
                                    x: weed.x/imageWidth*imageDrawSize.width + imageStartPoint.x,
                                    y: weed.y/imageHeight*imageDrawSize.height + imageStartPoint.y,
                                    width: markSize,
                                    height: markSize,
                                    stroke: config.WEED_MARKER_COLOR,
                                    strokeWidth: config.MARKER_STROKE_WIDTH
                                });
                                layer.add(weed.shape);
                            });
                            layer.draw();
                            // double click to reset the zoom and position
                            layer.on('dbltap dblclick', function(param){
                                if(param.target === backgroundRect){
                                    layer.setPosition({x: 0, y: 0});
                                    layer.scale({x: 1, y: 1});
                                    layer.draw();
                                }
                            });
                            var dragStartPoint = null;
                            // remember the start drag point
                            layer.on('dragstart', function(param){
                                var canvasOffset = $(".kineticjs-content").offset();
                                dragStartPoint = getEventOffset(param.evt, canvasOffset);
                            });
                            // handle the tap click and dragend event to mark stalk and weed
                            layer.on('tap click dragend', function(param){
                                var layerOffset = layer.getPosition();
                                var layerScale = layer.getScale().x;
                                var canvasOffset = $(".kineticjs-content").offset();
                                var cursorOffset = getEventOffset(param.evt, canvasOffset);
                                if(!cursorOffset){
                                    return;
                                }
                                // the position in image pixel coordinate
                                var position = {
                                    x: ((cursorOffset.x - layerOffset.x)/layerScale - imageStartPoint.x)/imageDrawSize.width*imageWidth,
                                    y: ((cursorOffset.y - layerOffset.y)/layerScale - imageStartPoint.y)/imageDrawSize.height*imageHeight
                                };
                                if(position.x < 0 || position.x > imageWidth || position.y < 0 || position.y > imageHeight){
                                    return;
                                }
                                if(param.type === 'dragend' && dragStartPoint){
                                    // when drag a small amount, also mark
                                    if(Math.abs(cursorOffset.x - dragStartPoint.x) > config.DRAG_AMOUNT_TREAT_AS_CLICK ||
                                        Math.abs(cursorOffset.y - dragStartPoint.y) > config.DRAG_AMOUNT_TREAT_AS_CLICK){
                                        return;
                                    }
                                    dragStartPoint = null;
                                }
                                var existMarkerIndex = -1;
                                var existMarker = null;
                                if(scope.markingStalks){
                                    // find whether click inside an exist marker
                                    existMarkerIndex = findShapeAtPoint(position, scope.image.mark.stalks, markSize*imageScale);
                                    existMarker = scope.image.mark.stalks[existMarkerIndex];
                                    if(existMarker){
                                        // remove the marker if click inside an exist marker
                                        existMarker.shape.remove();
                                        scope.image.mark.stalks.splice(existMarkerIndex, 1);
                                    } else {
                                        // add new marker
                                        position.shape = new Kinetic.Rect({
                                            x: (cursorOffset.x - layerOffset.x)/layerScale - markSize/2,
                                            y: (cursorOffset.y - layerOffset.y)/layerScale - markSize/2,
                                            width: markSize,
                                            height: markSize,
                                            stroke: config.STALK_MARKER_COLOR,
                                            strokeWidth: config.MARKER_STROKE_WIDTH
                                        });
                                        layer.add(position.shape);
                                        scope.image.mark.stalks.push(position);
                                    }
                                }
                                if(scope.markingWeeds){
                                    // find whether click inside an exist marker
                                    existMarkerIndex = findShapeAtPoint(position, scope.image.mark.weeds, markSize*imageScale);
                                    existMarker = scope.image.mark.weeds[existMarkerIndex];
                                    if(existMarker){
                                        // remove the marker if click inside an exist marker
                                        existMarker.shape.remove();
                                        scope.image.mark.weeds.splice(existMarkerIndex, 1);
                                    } else {
                                        // add new marker
                                        position.shape = new Kinetic.Circle({
                                            x: (cursorOffset.x - layerOffset.x)/layerScale,
                                            y: (cursorOffset.y - layerOffset.y)/layerScale,
                                            width: markSize,
                                            height: markSize,
                                            stroke: config.WEED_MARKER_COLOR,
                                            strokeWidth: config.MARKER_STROKE_WIDTH
                                        });
                                        layer.add(position.shape);
                                        scope.image.mark.weeds.push(position);
                                    }
                                }
                                layer.draw();
                                scope.$apply();
                            });                       
                        };
                    }
                    /**
                     * Get the event's cursor coordinate relative to container
                     * @param {Object} evt the event params
                     * @param {Object} containerOffset the container offset relative to page
                     * @returns {Object} the event coordinate relative to container 
                     */
                    function getEventOffset(evt, containerOffset){
                        if(evt.changedTouches && evt.changedTouches.length >= 1){
                            return {
                                x: evt.changedTouches[0].pageX - containerOffset.left,
                                y: evt.changedTouches[0].pageY - containerOffset.top
                            };
                        } else if(typeof evt.offsetX === 'number' && typeof evt.offsetY === 'number' ) {
                            return {
                                x: evt.offsetX,
                                y: evt.offsetY
                            };
                        } else {
                            return null;
                        }
                    }
                }
            };
        });
})();