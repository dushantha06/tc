/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * Represents the image upload page for admin
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */

(function() {
    'use strict';

    angular.module('app')
        .config(function($stateProvider) {
            $stateProvider
                .state('imageUpload', {
                    admin: true,
                    url: '^/image-upload',
                    templateUrl: 'app/image-upload/image-upload.html',
                    controller: 'ImageUploadCtrl as vm'
                });
        })
        .controller('ImageUploadCtrl', function(FileUploader, config, util) {
            var vm = this;
            var uploader = vm.uploader = new FileUploader({
                url: 'image-upload'
            });
            // image file type filter
            uploader.filters.push({
                name: 'imageFilter',
                fn: function(item, options) {
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                }
            });
            // image file size filter
            uploader.filters.push({
                name: 'sizeFilter',
                fn: function(item) {
                    return item.size < config.MAX_IMAGE_SIZE * 1024 * 1024;
                }
            });
            uploader.onWhenAddingFileFailed = function(item, filter, options) {
                if(filter.name === 'sizeFilter'){
                    util.showAlert('Info', 'The file ' + item.name + ' not selected because its size exceed the limit size ' + config.MAX_IMAGE_SIZE + 'MB');
                }
            };
        })
        .directive('ngcThumb', function($window) {
            var helper = {
                support: !!($window.FileReader && $window.CanvasRenderingContext2D),
                isFile: function(item) {
                    return angular.isObject(item) && item instanceof $window.File;
                },
                isImage: function(file) {
                    var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                }
            };

            return {
                restrict: 'A',
                template: '<canvas/>',
                link: function(scope, element, attributes) {
                    if (!helper.support) return;

                    var params = scope.$eval(attributes.ngcThumb);

                    if (!helper.isFile(params.file)) return;
                    if (!helper.isImage(params.file)) return;

                    var canvas = element.find('canvas');
                    var reader = new FileReader();

                    reader.onload = onLoadFile;
                    reader.readAsDataURL(params.file);

                    function onLoadFile(event) {
                        var img = new Image();
                        img.onload = onLoadImage;
                        img.src = event.target.result;
                    }

                    function onLoadImage() {
                        var width = params.width || this.width / this.height * params.height;
                        var height = params.height || this.height / this.width * params.width;
                        canvas.attr({ width: width, height: height });
                        canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                    }
                }
            };
        });
})();