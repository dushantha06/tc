/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * Represents the REST API service
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */

(function () {
    'use strict';

    angular.module('app')
        .factory('API', function ($resource) {
            return $resource('/', {}, {
                login: {
                    api: true,
                    method: 'POST',
                    url: '/login'
                },
                getImages: {
                    api: true,
                    method: 'GET',
                    url: '/images'
                },
                getImage: {
                    api: true,
                    method: 'GET',
                    url: '/image/:id'
                },
                uploadMarks: {
                    api: true,
                    method: 'POST',
                    url: '/mark/:id'
                },
                uploadImage: {
                    api: true,
                    method: 'POST',
                    url: '/image/:imageName'
                }
            });
        });
})();