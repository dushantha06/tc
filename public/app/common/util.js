/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * Represents the util service
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */

(function () {
    'use strict';

    angular.module('app')
        .factory("util", function ($uibModal, $log) {
            var util = {
                /**
                 * Show alert message
                 * @param title the alert title
                 * @param msg the message
                 * @returns {Object} the modal instance
                 */
                showAlert: function (title, msg) {
                    return $uibModal.open({
                        templateUrl: 'app/common/popups/alert.html',
                        controller: function ($scope) {
                            $scope.title = title; 
                            $scope.msg = msg; 
                        },
                        size: "md"
                    });
                }
            };
            return util;
        });
})();