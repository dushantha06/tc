/*
 * Copyright (c) 2015 TopCoder, Inc. All rights reserved.
 */

/**
 * This is the web server script.
 * @author TCSASSEMBLER
 * @version 1.0
 */
"use strict";

var config = require('config');
var helper = require('./common/helper');
var express = require('express');
var winston = require('winston');
var path = require('path');
var BadRequestError = require('./common/errors').BadRequestError;
var InternalError = require('./common/errors').InternalError;
var UnauthorizedError = require('./common/errors').UnauthorizedError;
var fs = require('fs');
var _ = require('underscore');
var bodyParser = require('body-parser');
var logger = require('./common/logger');
var createDomain = require('domain').create;
var multer  = require('multer')

var imagesDirectory = path.join(__dirname, '../images');
var marksDirectory = path.join(__dirname, '../marks');
var imageFiles = fs.readdirSync(imagesDirectory);
var markeFiles = fs.readdirSync(marksDirectory);
var unMarkedImages = [];
var currentImageId = 0;
var images = [];

// init the images and marks
_.each(imageFiles, function(imageFile){
    var id = helper.getId(imageFile);
    if(id > 0){
        var image = {
            id: id,
            fileName: imageFile,
            name: imageFile.slice(imageFile.indexOf('_') + 1)
        };
        var fileNameWithoutExt = helper.getFileNameWithoutExt(imageFile);
        var mark = _.find(markeFiles, fileNameWithoutExt + '.json');
        if(!mark){
            unMarkedImages.push(image);
        } else {
            var markPath = path.join(marksDirectory, fileNameWithoutExt + '.json');
            var markInfo = fs.readFileSync(markPath);
            try{
                markInfo = JSON.parse(markInfo);
            } catch(err){
                markInfo = null;
            }
            image.mark = markInfo;
        }
        images.push(image);
        if(image.id > currentImageId){
            currentImageId = image.id;
        }
    }
});
// sort the images by id
images = _.sortBy(images, 'id');

var app = express();
app.set('port', config.WEB_SERVER_PORT);
// serve static file folders
app.use(express.static(path.join(__dirname, '../public')));
// app.use('/marks', express.static(marksDirectory));
app.use('/images', express.static(imagesDirectory));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// catch uncaught error
app.use(function(req, res, next) {
    var domain = createDomain();
    domain.add(req);
    domain.add(res);
    domain.run(function() {
        next();
    });
    domain.on('error', function(e) {
        next(e);
    });
});

// endpoint for login
app.post('/login', function(req, res, next){
    var username = req.body.username;
    var password = req.body.password;
    winston.info('Login with username %s', username);
    if(!username || !password){
        next(new UnauthorizedError('The username and password must be provided to login'));
    } else {
        res.json({
            username: username,
            admin: username.toLowerCase() === 'admin'
        });      
    }
});

// endpoint for get image list
app.get('/images', function(req, res, next){
    var offset = parseInt(req.query.offset);
    var limit = parseInt(req.query.limit);
    winston.info('Get image list offset=%d limit=%d', offset, limit);
    if(offset >= 0 && limit > 0){
        res.json({
            total: images.length,
            items: images.slice(offset, offset + limit)
        });
    } else {
        next(new BadRequestError('The offset and limit params must be positive'));        
    }
});

// endpoint for get specific image
app.get('/image/:id?', function(req, res, next){
    var imageId = parseInt(req.params.id);
    winston.info('Get image imageId=%d', imageId);
    var image = null;
    if(!imageId){
        if(unMarkedImages.length === 0){
            return res.json({left: 0, image: null});
        } else {
            image = _.sample(unMarkedImages);
        }
    } else {
        image = _.find(images, function(image){
            return image.id === imageId;
        });
        if(!image){
            return next(new BadRequestError('The image not exist'));
        }
    }
    res.json({left: unMarkedImages.length, image: image});
});

var storage = multer.diskStorage({
    destination: imagesDirectory,
    filename: function(req, file, cb) {
        file.id = ++currentImageId;
        cb(null, file.id + '_' + file.originalname);
    }
});
var upload = multer({ storage: storage});
// endpoint for upload images
app.post("/image-upload", upload.array('file'), function(req, res, next){
    winston.info('Upload image files: %s', JSON.stringify(_.pluck(req.files, 'originalname')));
    _.each(req.files, function(file){
        var image = {
            id: file.id,
            name: file.originalname,
            fileName: file.filename
        };
        images.push(image);
        unMarkedImages.push(image);
    });
    res.end();
});

// endpoint for save mark info to JSON file
app.post("/mark/:id", function(req, res, next) {
    var imageId = parseInt(req.params.id);
    winston.info('Upload image marks, imageId=%d', imageId);    
    var markInfo = req.body;
    var image = _.find(images, function(image){
        return image.id === imageId;
    });
    if(image){
        fs.writeFile(path.join(marksDirectory, helper.getFileNameWithoutExt(image.fileName) + '.json'), JSON.stringify(markInfo),
            function(err) {
                if (err) {
                    return next(new InternalError('Error when write the mark information: ' + err.messgae));
                } else {
                    image.mark = markInfo;
                    var index = _.findIndex(unMarkedImages, image);
                    if(index >= 0){
                        unMarkedImages.splice(index ,1);
                    }
                    return res.end();
                }
            });
    } else {
        return next(new BadRequestError('The image not exist'));
    }
});

app.use(function (req, res) {
    res.status(404).json({error: "route not found"});
});

app.use(function (err, req, res, next) {
    logger.logFullError(err, req.method + " " + req.url);
    res.status(err.httpStatus || 500).json({
        error: err.message
    });
});

app.listen(app.get('port'), function () {
    winston.info('Express server listening on port %d', app.get('port'));
});