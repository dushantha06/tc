/*
 * Copyright (c) 2015 TopCoder, Inc. All rights reserved.
 */
"use strict";

/**
 * This file defines helper methods
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */

var fs = require("fs");
var BadRequestError = require('./errors').BadRequestError;

/**
 * Get the file name without extension
 * @param {String} fileName the file name with extension
 * @returns {String} the file name without extension
 */
function getFileNameWithoutExt(fileName) {
    var extIndex = fileName.lastIndexOf('.');
    if(extIndex >= 0){
        return fileName.slice(0, extIndex);
    } else {
        return fileName;
    }
}

/**
 * Get the id from file name
 * @param {String} fileName the file name with extension
 * @returns {Number} the id
 */
function getId(fileName) {
    var idIndex = fileName.indexOf('_');
    if(idIndex >= 0){
        return parseInt(fileName.slice(0, idIndex));
    } else {
        return NaN;
    }
}

/**
 * Get the JSON object from file
 * @param {String} path the file path
 * @param {Function(err, result)} callback the callback function
 */
function getJSONFromFile(path, callback){
    fs.exists(path, function (exists) {
        if(!exists){
            return callback(new BadRequestError('The file does not exist'));
        } else {
            fs.readFile(path, function (err, data) {
               return callback(err && new BadRequestError('Error when read file: ' + path), data);
            })
        }
    });
}

module.exports = {
    getFileNameWithoutExt: getFileNameWithoutExt,
    getId: getId,
    getJSONFromFile: getJSONFromFile
};